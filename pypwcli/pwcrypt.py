import base64
import os

from cryptography.fernet import Fernet, InvalidToken
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC


def patch_crypto_be_discovery():
    """
    Monkey patches cryptography's backend detection. Since its broken on windows for now
    Objective: support pyinstaller freezing.
    """

    from cryptography.hazmat import backends

    try:
        from cryptography.hazmat.backends.commoncrypto.backend import backend as be_cc
    except ImportError:
        be_cc = None

    try:
        from cryptography.hazmat.backends.openssl.backend import backend as be_ossl
    except ImportError:
        be_ossl = None

    backends._available_backends_list = [be for be in (be_cc, be_ossl) if be is not None]


# https://github.com/pyinstaller/pyinstaller/issues/2013
patch_crypto_be_discovery()


class EncryptedData:
    def __init__(self, data, salt):
        self.data = data
        self.salt = salt


class PwCryptException(Exception):
    pass


class BadPasswordException(PwCryptException):
    pass


def _generate_salt():
    return os.urandom(16)


def _get_key(password, salt):
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=salt,
        iterations=100000,
        backend=default_backend())
    return base64.urlsafe_b64encode(kdf.derive(password.encode('utf-8')))


def encrypt(content: str, password: str) -> EncryptedData:
    # Based off https://cryptography.io/en/latest/fernet/
    salt = _generate_salt()
    f = Fernet(_get_key(password, salt))
    data = f.encrypt(content.encode('utf-8'))
    return EncryptedData(data, salt)


def decrypt(data: EncryptedData, password: str) -> str:
    # Based off https://cryptography.io/en/latest/fernet/
    f = Fernet(_get_key(password, data.salt))
    # noinspection PyCompatibility
    try:
        return f.decrypt(data.data).decode('utf-8')
    except InvalidToken as e:
        raise BadPasswordException from e


def encode(encrypted: bytes) -> str:
    return base64.b64encode(encrypted).decode('utf-8')


def decode(serialized: str) -> bytes:
    return base64.b64decode(serialized)
