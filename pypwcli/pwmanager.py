import time
import random
from pypwcli.pwcrypt import decode, decrypt, encode, encrypt, EncryptedData

_RECORD_SEPERATOR = '\x1D'  # http://www.asciitable.com/
_FIELD_SEPERATOR = '\x1E'


class Password:
    _DEFAULT_PW_ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
    _RNG = random.SystemRandom()

    @classmethod
    def generate_password(cls, *required_buckets, minlen=10, maxlen=30, special_alphabet='') -> str:
        """
        Idea is we'll generate a password up to (maxlen - number of required buckets) then just
        fill out of the bucket for whatever is not filled but required

        When we run over buckets at end, if the bucket is already filled (there is a character that meets it) then
        just pull a random character

        Finally shuffle output so special characters aren't always at end
        """
        candidate_alphabet = cls._DEFAULT_PW_ALPHABET + special_alphabet
        num_buckets = len(required_buckets)

        def get_random_chr(candidates=candidate_alphabet):
            return candidates[cls._RNG.randint(0, len(candidates) - 1)]

        generated_pw = [get_random_chr() for _ in
                        range(minlen, maxlen - num_buckets + 1)]  # +1 to get range [a, b] from [a, b)

        generated_pw_crs = set(generated_pw)
        for bucket in required_buckets:
            alphabet = candidate_alphabet if set(bucket) & generated_pw_crs else bucket
            generated_pw.append(get_random_chr(alphabet))
        random.shuffle(generated_pw, lambda: cls._RNG.random())
        return ''.join(generated_pw)

    def __init__(self, site, username, password, date_created=None):
        self.site = site
        self.username = username
        self.password = password
        if not date_created:
            self.date_created = int(time.time())
        else:
            self.date_created = date_created

    def __repr__(self):
        return _FIELD_SEPERATOR.join(map(str, (self.site, self.username, self.password, self.date_created)))

    def __str__(self):
        return self.__repr__()

    def __eq__(self, other):
        return isinstance(other, Password) and str(other) == str(self)


class PasswordManager:
    def __init__(self, master_pw, backing_filename):
        self._master_pw = master_pw
        self._backing_filename = backing_filename
        self._passwords = dict()  # (site, username) -> Password

    def update_password(self, site, username, password, date_created=None):
        self._passwords[(site, username)] = Password(site, username, password, date_created)

    def list_logins(self) -> [Password]:
        return self._passwords.values()

    def get_details(self, site, username) -> Password:
        return self._passwords.get((site, username))

    def drop_details(self, site, username) -> None:
        if (site, username) in self._passwords:
            del self._passwords[(site, username)]

    def save(self):
        self.save_to_file(self._backing_filename)

    def save_to_file(self, filename):
        with open(filename, 'w+') as output:
            data = str(self)
            if data:
                output.write(data)

    @classmethod
    def load_from_file(cls, filepath, master_pw):
        manager = PasswordManager(master_pw, filepath)
        with open(filepath, 'r') as f:
            serialized = f.read().strip()
            if serialized:
                for entry in serialized.split(_RECORD_SEPERATOR):
                    ecipher, esalt = entry.split(_FIELD_SEPERATOR)
                    cipher, salt = decode(ecipher), decode(esalt)
                    cleartext = decrypt(EncryptedData(cipher, salt), master_pw)
                    site, username, site_password, date_created = cleartext.split(_FIELD_SEPERATOR)
                    manager.update_password(site, username, site_password, int(date_created))
        return manager

    def __str__(self):
        if self._passwords:
            encrypted_pws = [encrypt(str(p), self._master_pw) for p in self._passwords.values()]
            output_passwords = [_FIELD_SEPERATOR.join(
                (encode(e.data), encode(e.salt))) for e in encrypted_pws]
            password_data = _RECORD_SEPERATOR.join(output_passwords)
            return password_data
        else:
            return ''


if __name__ == '__main__':
    # print(Password.generate_password('~', '.', ',', special_alphabet='!@#$%^&*()'))
    PasswordManager.load_from_file('foo2', 'foo')
