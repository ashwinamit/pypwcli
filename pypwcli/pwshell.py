#!/usr/bin/env python
import cmd
import csv
import os
import signal

import click
import sys

import pyperclip
import tabulate

from pypwcli.pwcrypt import BadPasswordException
from pypwcli.pwmanager import PasswordManager, Password
from pypwcli.util import format_ts

EXIT_OK = 0
EXIT_BAD_PASSWORD = 1


class PasswordShell(cmd.Cmd):
    intro = 'Password Manager'
    prompt = '(pw) '

    def __init__(self, pwmanager: PasswordManager, **kwargs):
        super(PasswordShell, self).__init__(**kwargs)
        self._pwmanager = pwmanager
        self._unsaved_changes = False

    def do_list(self, site_filter):
        """
        List passwords. Optionally include filter to match sites
        Usage:
          list [filter]
        """
        passwords = sorted(self._pwmanager.list_logins(), key=lambda p: p.site)
        if site_filter:
            filtered_passwords = [e for e in passwords if site_filter in e.site]
        else:
            filtered_passwords = passwords
        password_records = [(p.site, p.username, format_ts(p.date_created)) for p in filtered_passwords]
        print(tabulate.tabulate(password_records, ['Site', 'Username', 'Created']))

    def do_add(self, arg):
        """
        Add a new password
        Usage:
          add site username password
        """
        args = arg.strip().split(' ', 2)
        if len(args) != 3:
            print('Enter 3 space seperated arguments. Args {} are not valid'.format(arg))
            return
        site, username, password = args
        self._unsaved_changes = True
        self._pwmanager.update_password(site, username, password)
        print('Success! Added password for {} {}'.format(site, username))

    def do_get(self, arg):
        """
        Get the details for a site/password
        Usage:
          details $site $username
        """
        args = arg.strip().split(' ')
        if len(args) != 2:
            print('Args {} not valid. Enter 2 space separated arguments'.format(arg))
            return
        site, username = args
        pw = self._pwmanager.get_details(site, username)
        print(tabulate.tabulate([[pw.site, pw.username, pw.password, pw.date_created]],
                                headers=['Site', 'Username', 'PW', 'Created']))
        pyperclip.copy(pw.password)

    def complete_get(self, text, line, begidx, endidx):
        if not text:
            return [p.site for p in self._pwmanager.list_logins()]
        if len(line.split(' ', 2)) < 3:
            return [p.site for p in self._pwmanager.list_logins() if p.site.startswith(text)]
        else:
            cmd, site, _ = line.split(' ', 2)
            return [p.username for p in self._pwmanager.list_logins() if p.site == site and p.username.startswith(text)]

    def do_drop(self, arg):
        args = arg.strip().split(' ')
        if len(args) != 2:
            print('Args {} not valid. Enter 2 space separated arguments'.format(arg))
            return
        site, username = args
        self._pwmanager.drop_details(site, username)

    def do_save(self, arg):
        if self._unsaved_changes:
            self._pwmanager.save()
            self._unsaved_changes = False

    def do_generate(self, arg):
        """
        Generates a password. Can specify minlen, maxlen, and the alphabet of special characters
        Usage:
          generate minlen maxlen special [buckets*]
        """
        args = arg.split(' ')
        if len(args) >= 3:
            minlen, maxlen, alphabet = args[0:3]
            buckets = args[3:]
        else:
            minlen, maxlen, alphabet = 10, 30, '!@#$%^&*()<>:"{}'
            buckets = []

        generated = Password.generate_password(*buckets, minlen=int(minlen), maxlen=int(maxlen),
                                               special_alphabet=alphabet)
        print(generated)
        pyperclip.copy(generated)

    def do_quit(self, arg):
        self.do_save(None)
        print('Goodbye!')
        sys.exit(EXIT_OK)


@click.command()
@click.argument('pwfile', envvar='PYPWCLI_FILE')
@click.option('--password', prompt=True, hide_input=True, required=True)
@click.option('--dump', is_flag=True, default=False, help='Dump passwords to stdout')
@click.option('--create', is_flag=True, default=False, help='Create pwfile if it does not exist')
def run_shell(pwfile, password, dump, create):
    if not os.path.isfile(pwfile) and create:
        with open(pwfile, 'w+'):
            pass  # create file

    try:
        pwmanager = PasswordManager.load_from_file(pwfile, password)
    except BadPasswordException:
        print('Error trying to decrypt file')
        sys.exit(EXIT_BAD_PASSWORD)

    def signal_handler(signame):
        def handler(signum_unused, frame_unused):
            pwmanager.save()
            sys.exit(EXIT_OK)

        return handler

    signal.signal(signal.SIGINT, signal_handler('sigint'))
    signal.signal(signal.SIGTERM, signal_handler('sigterm'))

    if dump:
        pws = pwmanager.list_logins()
        writer = csv.writer(sys.stdout)
        for pw in pws:
            writer.writerow([pw.site, pw.username, pw.password, pw.date_created])
    else:
        try:
            PasswordShell(pwmanager).cmdloop()
        except:  # not using finally because we normally save on normal exit so no need to double save
            pwmanager.save()
            raise


if __name__ == '__main__':
    run_shell()
