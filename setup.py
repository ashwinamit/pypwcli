from setuptools import setup

setup(name='pypwcli',
      version='1.0',
      description='',
      url='http://example.com',
      author='Ashwin Amit',
      author_email='ashwinamit@gmail.com',
      packages=['pypwcli'],
      test_suite='nose.collector',
      tests_require=['nose'],
      entry_points={
          'console_scripts': ['pypwcli=pypwcli.pwshell:run_shell'],
      },
      install_requires=[
          'cffi==1.8.2',
          'click==6.6',
          'cryptography==1.5',
          'idna==2.1',
          'pyasn1==0.1.9',
          'pycparser==2.14',
          'pyreadline==2.1',
          'pyperclip==1.5.27',
          'six==1.10.0',
          'tabulate==0.7.5',
      ],
      )
