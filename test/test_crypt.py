import unittest
from pypwcli.pwcrypt import (
    encrypt, encode, decrypt, decode,
    BadPasswordException
)


class CryptTestCase(unittest.TestCase):
    def test_crypt_identity(self):
        password = 'foo'
        data = 'bar'
        encrypted = encrypt(data, password)
        decrypted = decrypt(encrypted, password)
        self.assertEqual(data, decrypted)

    def test_encode_identity(self):
        data = b'foo'
        self.assertEqual(data, decode(encode(data)))

    def test_bad_pw(self):
        password = 'foo'
        data = 'bar'
        encrypted = encrypt(data, password)
        with self.assertRaises(BadPasswordException):
            decrypt(encrypted, password + '_')
