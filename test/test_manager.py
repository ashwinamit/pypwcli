import os
import tempfile
import unittest
from unittest.mock import patch

from pypwcli.pwmanager import PasswordManager, Password


class ManagerTestCase(unittest.TestCase):
    def setUp(self):
        self.pw = 'foo'
        self.backing_name = 'bar'
        self.manager = PasswordManager(self.pw, self.backing_name)

    def test_add(self):
        site, username, password = 'site', 'username', 'pw'
        self.assertIsNone(self.manager.get_details(site, username))
        with patch('time.time') as mock_time:
            now = 42
            mock_time.return_value = now
            self.manager.update_password(site, username, password)
            expected_pw = Password(site, username, password, now)
            self.assertEqual(expected_pw, self.manager.get_details(site, username))
            self.assertEqual(1, len(self.manager.list_logins()))

    def test_update(self):
        site, username, password_orig = 'site', 'username', 'old'
        self.manager.update_password(site, username, password_orig)
        entry = self.manager.get_details(site, username)
        self.assertIsNotNone(entry)
        password_new = 'new'
        entry_new = self.manager.update_password(site, username, password_new)
        self.assertNotEqual(entry, entry_new)
        self.assertEqual(1, len(self.manager.list_logins()))

    def test_drop(self):
        site, username, password = 'site', 'username', 'pw'
        self.manager.update_password(site, username, password)
        self.assertIsNotNone(self.manager.get_details(site, username))
        self.assertEqual(1, len(self.manager.list_logins()))
        self.manager.drop_details(site, username)
        self.assertIsNone(self.manager.get_details(site, username))
        self.assertEqual(0, len(self.manager.list_logins()))

    def test_multi_username(self):
        site = 'site'
        users = ['user_b', 'user_c']
        for u in users:
            self.manager.update_password(site, u, u + '_password')
        self.assertEqual(2, len(self.manager.list_logins()))
        for p in self.manager.list_logins():
            self.assertEqual(p.password, p.username + '_password')

    def test_str(self):
        self.assertEqual('', str(self.manager))

    def test_save(self):
        f = tempfile.NamedTemporaryFile(delete=True)
        f.close()
        pw = PasswordManager('foo', f.name)
        pw.update_password('site', 'username', 'password')
        pw.save()
        pw2 = PasswordManager.load_from_file(f.name, 'foo')
        self.assertEqual(1, len(pw2.list_logins()))
        self.assertEqual(pw.get_details('site', 'username'), pw2.get_details('site', 'username'))
        os.unlink(f.name)
