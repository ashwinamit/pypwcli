import unittest

from pypwcli.pwmanager import Password


class PasswordTestCase(unittest.TestCase):
    def test_gen(self):
        pw = Password.generate_password(minlen=1, maxlen=1)
        self.assertEqual(1, len(pw))

    def test_required_buckets(self):
        pw = Password.generate_password('a', 'b', 'c')
        self.assertTrue('a' in pw)
        self.assertTrue('b' in pw)
        self.assertTrue('c' in pw)

    def test_bucket_alphabet(self):
        pw = Password.generate_password('~')
        self.assertTrue('~' in pw)
