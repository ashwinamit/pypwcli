import unittest
from pypwcli.util import format_ts


class UtilTestCase(unittest.TestCase):
    def test_ts(self):
        ts = 1473327943  # 09/08/2016 @ 2:45am (UTC)
        formatted_ts = format_ts(ts)
        self.assertEqual('2016-09-08 02:45:43', formatted_ts)
